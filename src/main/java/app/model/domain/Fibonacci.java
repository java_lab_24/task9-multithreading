package app.model.domain;

public class Fibonacci {

  private static long recursiveFibo(int no) {
    if (no == 1 || no == 2) {
      return 1;
    }
    return recursiveFibo(no - 1) + recursiveFibo(no - 2);
  }

  public static class ParallelFibo extends Thread {

    private int x;
    private long answer;

    public ParallelFibo(int x) {
      this.x = x;
    }

    public int getX() {
      return x;
    }

    public long getAnswer() {
      return answer;
    }

    public void run() {
      if (x <= 2) {
        answer = 1;
      } else {
        try {
          ParallelFibo t = new ParallelFibo(x - 1);
          t.start();
          long y = recursiveFibo(x - 2);
          t.join();
          answer = t.answer + y;
        } catch (InterruptedException ex) {
        }
      }
    }
  }
}