package app.model.domain;

public class PingPong {

  private synchronized void ping() {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.print("Ping - ");
    try {
      wait();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private synchronized void pong() {
    try {
      Thread.sleep(1200);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("Pong!");
    notifyAll();
  }

  public void play(int times) {
    Thread pingThread;
    Thread pongThread;
    for (int i = 0; i < times; i++) {
      pingThread = new Thread(this::ping);
      pongThread = new Thread(this::pong);
      pingThread.start();
      pongThread.start();
      try {
        pingThread.join();
        pongThread.join();
      } catch (InterruptedException e) {}
    }
  }
}
