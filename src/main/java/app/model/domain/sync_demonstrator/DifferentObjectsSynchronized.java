package app.model.domain.sync_demonstrator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DifferentObjectsSynchronized {

  private static final Logger logger = LogManager.getLogger(DifferentObjectsSynchronized.class);
  Object lock1 = new Object();
  Object lock2 = new Object();
  Object lock3 = new Object();

  public void task1() {
    synchronized (lock1) {
      logger.info(Thread.currentThread().getName() + ": First task is in progress...");
      try {
        logger.info(Thread.currentThread().getName() + ": I am sleeping now");
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      logger.info(Thread.currentThread().getName() + ": First task finished!");
    }
  }

  public void task2() {
    synchronized (lock2) {
      logger.info(Thread.currentThread().getName() + ": Second task is in progress...");
      try {
        logger.info(Thread.currentThread().getName() + ": I am sleeping now");
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      logger.info(Thread.currentThread().getName() + ": Second task finished!");
    }
  }

  public void task3() {
    synchronized (lock3) {
      logger.info(Thread.currentThread().getName() + ": Third task is in progress...");
      try {
        logger.info(Thread.currentThread().getName() + ": I am sleeping now");
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      logger.info(Thread.currentThread().getName() + ": Third task finished!");
    }
  }
}
