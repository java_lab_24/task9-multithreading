package app.model.domain.sync_demonstrator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SameObjectSynchronized {

  private static final Logger logger = LogManager.getLogger(SameObjectSynchronized.class);

  public void task1() {
    synchronized (this) {
      logger.info(Thread.currentThread().getName() + ": First task is in progress...");
      try {
        logger.info(Thread.currentThread().getName() + ": I am sleeping now");
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      logger.info(Thread.currentThread().getName() + ": First task finished!");
    }
  }

  public void task2() {
    synchronized (this) {
      logger.info(Thread.currentThread().getName() + ": Second task is in progress...");
      try {
        logger.info(Thread.currentThread().getName() + ": I am sleeping now");
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      logger.info(Thread.currentThread().getName() + ": Second task finished!");
    }
  }

  public void task3() {
    synchronized (this) {
      logger.info(Thread.currentThread().getName() + ": Third task is in progress...");
      try {
        logger.info(Thread.currentThread().getName() + ": I am sleeping now");
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      logger.info(Thread.currentThread().getName() + ": Third task finished!");
    }
  }
}
