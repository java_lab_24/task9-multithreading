package app.model.domain;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PipedStream {

  private final static Logger logger = LogManager.getLogger(PipedInputStream.class);
  private final PipedOutputStream output = new PipedOutputStream();
  private final PipedInputStream input = new PipedInputStream(output);

  Thread thread1 = new Thread(new Runnable() {
    @Override
    public void run() {
      try {
        logger.info(Thread.currentThread().getName() + ": I have an apple");
        Thread.sleep(1000);
        logger.info(Thread.currentThread().getName() + ": I have a pan");
        Thread.sleep(1000);
        logger.info(Thread.currentThread().getName() + ": Hmmm, Ahhh!");
        Thread.sleep(1000);
        output.write("Pineapple pen".getBytes());
      } catch (IOException e) {
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  });


  Thread thread2 = new Thread(new Runnable() {
    @Override
    public void run() {
      try {
        logger.info(Thread.currentThread().getName() + ": waiting massage from other thread!");
        int data = input.read();
        logger.info(Thread.currentThread().getName() + ": just received it");
        while (data != -1) {
          logger.info(Thread.currentThread().getName() + ": " + (char) data);
          data = input.read();
        }
      } catch (IOException e) {
      }
    }
  });

  public PipedStream() throws IOException, InterruptedException {
    thread1.start();
    thread2.start();
    thread1.join();
    thread2.join();
  }
}
