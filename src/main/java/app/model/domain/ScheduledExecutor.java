package app.model.domain;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ScheduledExecutor {

  private final Random random;
  private CountDownLatch lock;

  public ScheduledExecutor(int quantity) {
    random = new Random();
    this.lock = new CountDownLatch(quantity);
    run();
  }

  private void run() {
    ScheduledExecutorService executor = Executors.newScheduledThreadPool(3);
    ScheduledFuture<?> future = executor.scheduleAtFixedRate(() -> {
      int sleepTime = random.nextInt(10);
      System.out.println("Scheduled thread slept to " + sleepTime);
      try {
        Thread.sleep(sleepTime);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      lock.countDown();
    }, 500, 100, TimeUnit.MILLISECONDS);

    try {
      lock.await(1000, TimeUnit.MILLISECONDS);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    future.cancel(true);
    executor.shutdown();
  }
}
