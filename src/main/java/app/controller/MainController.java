package app.controller;

import app.model.domain.Fibonacci;
import app.model.domain.PingPong;
import app.model.domain.PipedStream;
import app.model.domain.ScheduledExecutor;
import app.model.domain.sync_demonstrator.DifferentObjectsSynchronized;
import app.model.domain.sync_demonstrator.SameObjectSynchronized;
import app.view.MainView;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainController {

  private static final Logger logger = LogManager.getLogger(MainController.class);
  private final Scanner scn;
  private MainView view;
  private Map<String, String> menu;
  private Map<String, SolveAble> methodMenu;

  public MainController() {
    view = new MainView();
    scn = new Scanner(System.in);
    menu = new LinkedHashMap<>();
    menu.put("1", "Test Ping - Pong!");
    menu.put("2", "Solve Fibonacci sequence in multi thread mode");
    menu.put("3", "ScheduledThreadPool task");
    menu.put("4",
        "Solve tasks where methods are synchronized with the same object and with the different objects");
    menu.put("5", "Piped stream");
    menu.put("q", "Quit");
    methodMenu = new HashMap<>();
    methodMenu.put("1", this::pressButton1);
    methodMenu.put("2", this::pressButton2);
    methodMenu.put("3", this::pressButton3);
    methodMenu.put("4", this::pressButton4);
    methodMenu.put("5", this::pressButton5);
  }

  private void pressButton1() {
    new PingPong().play(2);
  }

  private void pressButton2() {
    final int fibonacciNthNumber = 40;
    logger.trace("Getting value of " + fibonacciNthNumber + "th fibonacci sequence");
    Fibonacci.ParallelFibo parallelFibo = new Fibonacci.ParallelFibo(fibonacciNthNumber);
    parallelFibo.run();
    logger.info("Value of " + fibonacciNthNumber + "th sequence = " + parallelFibo.getAnswer());
  }

  private void pressButton3() {
    final int taskRunQuantity = 5;
    logger.trace("Running 3 Scheduled Threads and putting them to sleep "
        + taskRunQuantity + " times in random time");
    new ScheduledExecutor(taskRunQuantity);
  }

  private void pressButton4() throws InterruptedException {
    logger.trace("Solving task where methods are synchronized with the same object");
    SameObjectSynchronized sameObjectSynchronized = new SameObjectSynchronized();
    ExecutorService executorsSame = Executors.newFixedThreadPool(3);
    executorsSame.submit(sameObjectSynchronized::task1);
    executorsSame.submit(sameObjectSynchronized::task2);
    executorsSame.submit(sameObjectSynchronized::task3);
    executorsSame.shutdown();
    executorsSame.awaitTermination(24L, TimeUnit.HOURS);
    logger.trace("Solving task where methods are synchronized with different objects");
    DifferentObjectsSynchronized differentObjectsSynchronized = new DifferentObjectsSynchronized();
    ExecutorService executorsDifferent = Executors.newFixedThreadPool(3);
    executorsDifferent.submit(differentObjectsSynchronized::task1);
    executorsDifferent.submit(differentObjectsSynchronized::task2);
    executorsDifferent.submit(differentObjectsSynchronized::task3);
    executorsDifferent.shutdown();
    executorsDifferent.awaitTermination(24L, TimeUnit.HOURS);
  }

  private void pressButton5() {
    try {
      new PipedStream();
    } catch (IOException | InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void start() {
    String menuInput;
    do {
      System.out.println();
      view.printMenu(menu);
      System.out.print("Please, select menu option: ");
      menuInput = scn.nextLine();
      try {
        methodMenu.get(menuInput).solve();
      } catch (Exception e) {
      }
    } while (!menuInput.equals("q"));
  }
}
