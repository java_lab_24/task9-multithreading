package app.view;

import java.util.Map;

public class MainView {

    public void printMenu(Map<String, String> map) {
    for (Map.Entry entry : map.entrySet()) {
      System.out.println("  " + entry.getKey() + "  " + entry.getValue());
    }
  }
}
